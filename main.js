/** Rain collector: Place collector sprite at bottom of canvas. **/

window.onload = myGame;
enchant();

function myGame() {
    // New game with a 320x320 pixel canavs, 24 frames/sec.
    var game = new Core(320, 320);
    game.fps = 24;

    // Preload assets.
    game.preload('droplet24.png');
    game.preload('usertrash32.png');

    // Specify what should happen when the game loads.
    game.onload = function () {

        // Gameplay parameters
        game.rootScene.backgroundColor = 'rgb(64,74,92)';
        var gravity = 6;    // Number of pixels per frame to move things down.
        
        // Droplet Sprite placed random x just above scene.
        var droplet = new Sprite(24, 24);
        droplet.image = game.assets['droplet24.png'];
        droplet.x = randomInt(0, game.width - droplet.width);
        droplet.y = -1 * droplet.height;
        game.rootScene.addChild(droplet);

        // Event listeners for droplet.
        droplet.addEventListener(Event.ENTER_FRAME, function () {
            if (droplet.y > game.height) {  // if droplet has gone below canvas,
                droplet.x = randomInt(0, game.width - droplet.width);  // new x,
                droplet.y = -1 * droplet.height;            // reset y location.
            } else {
                droplet.y += gravity;                     // move down as usual.
            }
        });

        // Rain collector Sprite placed at the bottom of the screen.
        var collector = new Sprite(32, 32);
        collector.image = game.assets['usertrash32.png'];
        collector.x = (game.width + collector.width) / 2;
        collector.y = game.height - collector.height;
        game.rootScene.addChild(collector);
    };
    
    // Start the game.
    game.start();
}

// === Helper functions === //
/** Generate a random integer between low and high (inclusive). */
function randomInt(low, high) {
    return low + Math.floor((high + 1 - low) * Math.random());
}
